const express = require("express");
const router = express.Router();
let orders = require("../models/orders");
let users = require("../models/users");
const getRandomInt = require("../aleatorio");
let status = require("../models/status");

//------------------ ORDERS ---------------//

/**
 * @swagger
 * /orders:
 *  get:
 *    description: orders
 *    tags:
 *    - orders
 *    summary: orders
 *    responses:
 *      200:
 *        description: Success
 */
router.get("/", function(req, res) {
    res.json({"orders": orders});
});

//----------------NEW ORDER------------------//

/**
 * @swagger
 * /orders:
 *  post:
 *    description: new order
 *    tags:
 *    - orders
 *    summary: new order
 *    parameters:
 *    - name: description
 *      description: description
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: adress
 *      description: adress 
 *      in: formData
 *      required: true
 *      type: string
 *    - name: payments
 *      description: payments
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.post("/", (req, res) => {
    
    let id_user = 0;

    var today = new Date();
    var day = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
    var hours = today.getHours() + ':' + today.getMinutes();

    const neworder = req.body;
    neworder.id_user = users[id_user].id;
    neworder.id = getRandomInt(1, 100);
    neworder.time = day + ' ' + hours;
    neworder.status = 1;
    orders.push(neworder);
    res.json({msj: `added order`});
});


//-----------------UPDATE---------------------//
/**
 * @swagger
 * /orders:
 *  put:
 *    description: update a orders
 *    tags:
 *    - orders
 *    summary: update a orders
 *    parameters:
 *    - name: id
 *      description: orders id
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: description
 *      description: description
 *      in: formData
 *      required: false
 *      type: string
 *    - name: adress
 *      description: adress
 *      in: formData
 *      required: false
 *      type: string
 *    - name: payments
 *      description: payments
 *      in: formData
 *      required: false
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put("/", (req, res) => {
    id_order = req.body.id;
    const indexorder = orders.findIndex(x => x.id == id_order);
    
    const object = {
      id: parseInt(req.body.id),
      id_user: req.body.id_user,
      description: req.body.description,
      adress: req.body.adress,
      payments: req.body.payments,
      };
      orders[indexorder] = object;
    
      res.json({msj: `update order`});
});

//-----------------DELETE----------------------//

/**
 * @swagger
 * /orders/{id}:
 *  delete:
 *    description: delete order
 *    tags:
 *    - orders
 *    summary: delete order
 *    parameters:
 *    - name: id
 *      description: order id
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete('/:id', (req, res) => {
    const id_order= 
    parseInt(req.params.id);
    orders = orders.filter(order => order.id != id_order);
      res.json({msj: `order removed`});
    });
    
module.exports = router;