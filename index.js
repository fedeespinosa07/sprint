const express = require('express');
const app = express();
const port = 3018;

const swaggerUI = require('swagger-ui-express');
const swaggerJSDoc = require('swagger-jsdoc');
const users = require('./routes/users');
const products = require('./routes/products');
const orders = require('./routes/orders');
const payments = require('./routes/payments');

// ------------- SETUP SWAGGER UI ----------------//

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'API restaurant Delilah',
      version: '1.0.0'
    }
  },
  apis: ["./routes/users.js","./routes/products.js", "./routes/orders.js", "./routes/payments.js"],
};

const swaggerDocs = swaggerJSDoc(swaggerOptions);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());


app.use('/api-docs',
  swaggerUI.serve,
  swaggerUI.setup(swaggerDocs));

// ------------------- ROUTES -------------------//

app.use('/users', users);

app.use('/products', products);

app.use('/orders', orders);

app.use('/payments', payments);

app.listen(port, () => {
  console.log(`Listening on port http://localhost:${port}`);
});