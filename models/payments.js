const payments = [
    {id: 1, payment: "cash"},
    {id: 2, payment: "credit card"},
    {id: 3, payment: "bitcoin"}
];

module.exports = payments;