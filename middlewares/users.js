let users = require("../models/users")

//------------------- LOGIN ------------------------//

function idLogin(req, res, next) {

    const login = users.find(
    (user) =>
      user.username === req.body.username && user.password === req.body.password
  );
  if (login) {
    login.status = true;
    next();
  } else {
    return res.status(500).json({msg: "Incorrect user or password" });
  }
};

//



module.exports = idLogin;