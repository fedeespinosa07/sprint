const express = require("express");
const router = express.Router();
let payments = require("../models/payments");

//------------------ PAYMENTS ---------------//

/**
 * @swagger
 * /payments:
 *  get:
 *    description: payments
 *    tags:
 *    - payments
 *    summary: payments
 *    responses:
 *      200:
 *        description: Success
 */
 router.get("/", function(req, res) {
    res.json({"payments": payments});
});

module.exports = router;