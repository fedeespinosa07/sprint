const status = [
    {id: 1, status: "pending"},
    {id: 2, status: "confirmed"},
    {id: 3, status: "in preparation"},
    {id: 4, status: "sent"},
    {id: 5, status: "delivered"}
];

module.exports = status;