const express = require("express");
const router = express.Router();
let users = require("../models/users");
const getRandomInt = require("../aleatorio");
const idLogin = require("../middlewares/users");

// --------------------- REGISTRATION -----------------------//

/**
 * @swagger
 * /users:
 *  post:
 *    description: registration
 *    tags:
 *    - users
 *    summary: registration
 *    parameters:
 *    - name: id
 *      description: user Id 
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: username
 *      description: username
 *      in: formData
 *      required: true
 *      type: string
 *    - name: name
 *      description: name
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: email
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      descrption: password
 *      in: formData
 *      required: true
 *      type: string
 *    - name: phone
 *      description: phone
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post('/', (req, res) => {
  const newUser = req.body;
  newUser.id = getRandomInt(1, 100);
  newUser.status = false;
  users.push(newUser);
  res.json({msj: `user added successfully`});
});


//------------------------ USERS -----------------------//

/**
 * @swagger
 * /users:
 *  get:
 *    description: list of all users
 *    tags:
 *    - users
 *    summary: list of all users
 *    responses:
 *      200:
 *        description: Success
 */
router.get("/", function (req, res) {
  res.json({"users": users});  
});

//---------------------- UPDATE A USER --------------------//

/**
 * @swagger
 * /users:
 *  put:
 *    description: update a user
 *    tags:
 *    - users
 *    summary: update a user
 *    parameters:
 *    - name: id
 *      description: user Id
 *      in: formData
 *      required: true
 *      type: integer
 *    - name: username
 *      description: username
 *      in: formData
 *      required: true
 *      type: string
 *    - name: name
 *      description: name
 *      in: formData
 *      required: true
 *      type: string
 *    - name: email
 *      description: email
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: password
 *      in: formData
 *      required: true
 *      type: string
 *    - name: phone
 *      description: phone 
 *      in: formData
 *      required: true
 *      type: string
 *    - name: status
 *      description: status
 *      in: formData
 *      required: false
 *      type: string 
 *    responses:
 *      200:
 *        description: Success
 */
 router.put('/', (req, res) => {
  id_user = req.body.id;
  const indexuser = users.findIndex(x => x.id == id_user);

  const object = {
    id: parseInt(req.body.id),
    username: req.body.username,
    name: req.body.name,
    email: req.body.email,
    password: req.body.password,
    phone: req.body.phone,
    status: req.body.status,

  };
  users[indexuser] = object;

  res.json({msj: `update user successfully`});
});


// --------------------- DELETED USER --------------------------//

/**
 * @swagger
 * /users/{id}:
 *  delete:
 *    description: delete a user
 *    tags:
 *    - users
 *    summary: delete a user
 *    parameters:
 *    - name: id
 *      description: user id
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete('/:id', (req, res) => {
  let id_user = 
  parseInt(req.params.id);
  users = users.filter(user => user.id != id_user);
  res.json({msj: `Deleted user`});
});

// ------------------- LOGIN --------------------- //

/**
 * @swagger
 * /users/login:
 *  post: 
 *    description: login
 *    tags:
 *    - name: login
 *    summary: login
 *    parameters:
 *    - name: username
 *      description: username
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      in: formData
 *      required: true
 *      type: string 
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: invalid username or password
 */
router.post('/login', idLogin, ( req, res, next) => {
  res.json({msg: 'Success'});
  next()
});

module.exports = router;

