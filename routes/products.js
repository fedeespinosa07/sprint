const express = require("express");
const router = express.Router();
let products = require("../models/products")
const getRandomInt = require("../aleatorio")

// ------------------- PRODUCTS --------------//

/**
 * @swagger
 * /products/:
 *  get:
 *    description: products
 *    tags:
 *    - products
 *    summary: products
 *    responses:
 *      200:
 *        description: Success
 */
router.get("/", function(req, res) {
    res.json({"products": products})
});

// ------------------- new products --------------------//

/**
 * @swagger
 * /products:
 *  post:
 *    description: new product
 *    tags:
 *    - products
 *    summary: new product
 *    parameters:
 *    - name: id
 *      description: product id
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: product name
 *      description: product name
 *      in: formData
 *      required: true
 *      type: string
 *    - name: price
 *      description: price 
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.post('/', (req, res) => {
    const newproduct = req.body;
    newproduct.id = getRandomInt(1, 100);
    products.push(newproduct);
    res.json({msj: `added product`});
  });
  
// ------------------ UPDATE A PRODUCTS ------------//

/**
 * @swagger
 * /products:
 *  put:
 *    description: update a product
 *    tags:
 *    - products
 *    summary: update a product
 *    parameters:
 *    - name: id
 *      description: product id
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: product
 *      description: product name
 *      in: formData
 *      required: true
 *      type: string
 *    - name: price
 *      description: price
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        description: Success
 */
router.put('/', (req, res) => {
  id_product = req.body.id;
  const indexproduct = products.findIndex(x => x.id == id_product);
  
  const object = {
    id: parseInt(req.body.id),
    product: req.body.product,
    price: req.body.price,
    };
    products[indexproduct] = object;
  
    res.json({msj: `update product`});
  });

//---------------- DELETED PRODUCT ------------//

/**
 * @swagger
 * /products/{id}:
 *  delete:
 *    description: delete product
 *    tags:
 *    - products
 *    summary: delete a product
 *    parameters:
 *    - name: id
 *      description: product id
 *      in: path
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 */
router.delete('/:id', (req, res) => {
const id_product= 
parseInt(req.params.id);
products = products.filter(product => product.id != id_product);
  res.json({msj: `product removed`});
});

module.exports = router;